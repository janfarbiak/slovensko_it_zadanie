Role Name
=========

A brief description of the role goes here.

Requirements
------------


Role Variables
--------------


Dependencies
------------


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    ---
    - hosts: all
    become: true

    roles: 
      - nginx
  
    vars_files:
      - ./ansible_ssh_vars.yml
      - ./ansible_vars.yml

License
-------


Author Information
------------------
Name: Jan Farbiak
