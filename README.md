## NGINX server on docker virtual machine using ansible playbook

### Requirements

> - linux OS based on debian (ubuntu, linux mint, debian)
> - docker with docker-compose

### Run Process

> 1. Download all files
> 2. go in bash inside the folder
> 3. run with cmd: `docker-compose up --build`
> 4. wait for the process end 
> 5. go to on linux station in browser: <localhost:8090> 

### <center>How it works

![The San Juan Mountains are beautiful!](/assets/postup.png "postup")

### End Process

> 1. `docker-compose down`
> 2. `docker stop nginx`
> 3. `docker rm nginx`
> 4. `docker system prune -a`
> 5. `docker builder prune`



